SRC:=src/kmain.amp src/vga.amp src/string.amp src/utils.amp
OBJ:=obj/kernel.amp.o obj/amd64.asm.o
LINKER:=linker.ld
KERNEL:=kernel.bin
ISO:=kernel.iso
ISODIR:=isodir

AMPFLAGS:=-O0 --debuginfo --include:src --package:lamp
ASFLAGS:=-f elf64

AMPC:=etc
AS:=nasm
CC:=gcc

.PHONY: default all clean mrproper

default: all

all: $(ISO)

run: $(ISO)
	qemu-system-x86_64 -cdrom $(ISO)

$(ISO): $(KERNEL) grub.cfg
	mkdir -p $(ISODIR)/boot/grub
	cp grub.cfg $(ISODIR)/boot/grub/
	cp $(KERNEL) $(ISODIR)/boot/
	grub-mkrescue -d /usr/lib/grub/i386-pc -o $@ $(ISODIR)

$(KERNEL): $(OBJ) $(LINKER)
	$(LD) -n -T $(LINKER) -o $@ $(OBJ)

obj/kernel.amp.o: $(SRC)
	$(AMPC) compile $(AMPFLAGS) --emit:obj -o:$@ $(SRC)

obj/%.asm.o: src/%.asm
	$(AS) $(ASFLAGS) -o $@ $^

clean:
	rm -f $(OBJ)
	rm -f $(KERNEL)

mrproper: clean
	rm -f $(ISO)
