bits 32

%define STACK_SIZE 16384
%define MB_MAGIC 0xe85250d6
%define MB_SIZE mboot.end - mboot.start
%define MB_CHECKSUM -(MB_MAGIC + 0 + MB_SIZE)

extern kmain

section .multiboot
align 4
mboot:
.start:
    dd MB_MAGIC
    dd 0 ; architecture 0 (protected i386)
    dd MB_SIZE
    dd MB_CHECKSUM
    ;; end tag
    dw 0 ; type
    dw 0 ; flags
    dd 8 ; size
.end:

section .text
global _start
_start:
.start:
    mov     esp, stack.end

    push    eax
    push    ebx

    jmp     set_long_mode
.end:
.size: equ $ - .start

set_long_mode:
.start:
.setup_page_tables:
    mov     eax, p3_table
    or      eax, 0x3 ; present | writable 
    mov     [p4_table], eax

    mov     eax, p2_table
    or      eax, 0x3 ; present | writable 
    mov     [p3_table], eax

    mov     ecx, 0
.map_p2:
    mov     eax, 0x200000
    mul     ecx
    or      eax, 0x83
    mov     [p2_table + ecx * 8], eax

    inc     ecx
    cmp     ecx, 512
    jne     .map_p2

    mov     eax, p4_table
    mov     cr3, eax

    mov     eax, cr4
    or      eax, 1 << 5 ; PAE
    mov     cr4, eax

    mov     ecx, 0xc0000080
    rdmsr
    or      eax, 1 << 8
    wrmsr

    mov     eax, cr0
    or      eax, 1 << 31
    mov     cr0, eax

    lgdt    [gdt.ptr]

    jmp     gdt.code:_start64
.end:
.size: equ $ - .start

bits 64
_start64:
.start:
    mov     ax, 0x0
    mov     ss, ax
    mov     ds, ax
    mov     es, ax
    mov     fs, ax
    mov     gs, ax

    mov     rsp, stack.end
    sub     rsp, 8

    pop     rax

    mov     rdx, 0xffffffff
    mov     rsi, rax
    shr     rsi, 32
    mov     rdi, rax
    and     rdi, rdx

    call    kmain

    cli
.halt:
    hlt
    jmp     .halt
.end:
.size: equ $ - .start

;; todo: unmangle these
global outb
global inb

global outw
global inw

global outd
global ind

outb:
.start:
    mov ax, si
    mov dx, di
    out dx, al
    ret
.end:
.size: equ $ - .start

inb:
.start:
    mov dx, di
    in al, dx
    ret
.end:
.size: equ $ - .start

outw:
.start:
    mov ax, si
    mov dx, di
    out dx, ax
    ret
.end:
.size: equ $ - .start

inw:
.start:
    mov dx, di
    in ax, dx
    ret
.end:
.size: equ $ - .start

outd:
.start:
    mov eax, esi
    mov dx, di
    out dx, eax
    ret
.end:
.size: equ $ - .start

ind:
.start:
    mov dx, di
    in eax, dx
    ret
.end:
.size: equ $ - .start

section .rodata
align 4
gdt:
    dq 0
.code: equ $ - gdt
    dq 1 << 43 | 1 << 44 | 1 << 47 | 1 << 53 ; code segment
.ptr:
    dw $ - gdt - 1
    dd gdt

section .bss
align 4096
p4_table:
    resb 4096
p3_table:
    resb 4096
p2_table:
    resb 4096

align 16
stack:
.start:
    resb STACK_SIZE
.end:
